#!/bin/bash

set -e

/data/open-c3/open-c3.sh upgrade
/data/open-c3/open-c3.sh sup
/data/open-c3/open-c3.sh dup
